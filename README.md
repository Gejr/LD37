# Ludum Dare 37

This is my game, i made for Ludum Dare 37. I have made this game in 48 hours solo. It is propperly an Un-word game because it's unfinished, unpolished, unadmirable, unbarring <bold>BUT</bold> it was fun to make!

<h2> This Ludum Dare theme was "One Room"  &mdash; (09-12-2016)</h2>
 
<h3> Introduction to the game</h3>
My Game is about RC plane dog fighting.
Currently the features included is:
<ul>
	<li> Enemy A.I</li>
	<li> Enemy Chasing after you + pathfinding</li>
	<li> Shooting, both Player and Enemy</li>
	<li> Menus </li>
	<li> Respawning</li>
	<li> Singelplayer</li>
	<li> Multiplayer</li>
</ul>

Known issues:<br>
<bold>Singelplayer</bold> When you die, and the Enemy AI dies while you are dead, the game will crash, and the same the other way around<br>
<i>Very</i> buggy multiplayer (last minute implementation) some of the bugs are, bad optimization, bad latency, sometimes other player's are invisible.<br>

<h3> Tools used</h3>
The tools i used to make this game, was the following
<ul>
	<li> Unity </li>
	<li> Visual Studio - Community 2015 </li>
	<li> Git bash</li>
	<li> LMMS</li>
	<li> sfxr</li>
	<li> Chronolapse</li>
	<li> HexChat</li>
</ul>

The tools "LMMS" & "sfxr" is used to make sound effects and music for the game, the tool "Chronolapse" is not used in the game, but it was used to take screenshots in an interval so i could make it into a video, and evaluate myself afterwards, last "HexChat" is a great IRC program used to chat with other people also participating in Ludum Dare 37, and was used to keep me motivated and inspiration <3