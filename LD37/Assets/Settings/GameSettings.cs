﻿/*
 * @summary			Settings, saving and loading
 * @description		Game options window in Unity 5 and saving the settings to the pc 
 * @version			1.0.0
 * @file			SettingManager.cs
 * @author			Malte Gejr 
 * @contact			www.mkkvk.dk/kontakt
 * 
 * @copyright		Copyright 2015-2016 Malte Gejr, all rights reserved.
 * 
 * This source file is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY.
 * 
 */

using UnityEngine;
using System.Collections;

public class GameSettings {

	public bool fullscreen;
	public int textureQuality;
	public int antialiasing;
	public int vSync;
	public int resolutionIndex;
	public float audioVolume;
}
