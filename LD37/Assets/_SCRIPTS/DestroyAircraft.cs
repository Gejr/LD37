﻿using UnityEngine;
using System.Collections;

public class DestroyAircraft : MonoBehaviour {

    public GameObject explosion;
    public GameObject enemySpawnSpot;
	public GameObject myPlayerGO;

    void OnTriggerEnter(Collider impactObject)
    {
		if(impactObject.tag != "ChaseSensor" && impactObject.tag != "EnemyAI")
        {
			NetworkManager nm = GameObject.FindObjectOfType<NetworkManager>();

			if(transform.parent.parent.name == "_PLAYER_CONTROLLER(Clone)")
			{
				//
				//Respawner Player, den gør det kun år Player dør
				//
				Debug.Log ("Player died, standbyCamera activated");
				nm.standbyCamera.SetActive(true);
				nm.respawnTimer = 5f;

			} else if(transform.parent.parent.name == "_ENEMY_AI(Clone)")
			{
				//
				//Respawner Enemy, HVIS det er offline mode, den gør det kun når enemy dør.
				//
				if(PhotonNetwork.offlineMode == true)
				{
					Debug.Log("Respawning OFFLINE Enemy");
					PhotonNetwork.Instantiate("_ENEMY_AI", enemySpawnSpot.transform.position, enemySpawnSpot.transform.rotation, 0);
					nm.AddChatMessage("New enemy target acquired");
				}
			} else
			{
				Debug.LogError ("Noting respawned");

			}       

			nm.AddChatMessage ("Enemy target: " + PhotonNetwork.player.name + " eliminated");
			Instantiate(explosion, transform.position, transform.rotation);
			PhotonNetwork.Destroy(transform.parent.parent.gameObject);
           
        }
    }
}
