﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Aeroplane;
using UnityStandardAssets.Utility;
using System.Collections.Generic;

public class NetworkManager : MonoBehaviour {

    public GameObject standbyCamera;
    public GameObject enemySpawnSpot;
    public GameObject enemyAI;
	public GameObject gameOptions;
    SpawnSpot[] spawnSpots;
    public bool offlineMode = false;
    bool connecting = false;
    List<string> chatMessages;
    int maxChatMessages = 5;
    public float respawnTimer = 0;

    // Use this for initialization
    void Start ()
    {
        spawnSpots = GameObject.FindObjectsOfType<SpawnSpot>();
        PhotonNetwork.player.name = PlayerPrefs.GetString("Username", "Guest " + Random.Range(1, 1000).ToString());
        chatMessages = new List<string>();
    }

    void OnDestroy()
    {
        PlayerPrefs.SetString("Username", PhotonNetwork.player.name);
    }

    public void AddChatMessage(string m)
    {
        GetComponent<PhotonView>().RPC("AddChatMessage_RPC", PhotonTargets.All, m);
    }

    [PunRPC]
    void AddChatMessage_RPC(string m)
    {
        while (chatMessages.Count >= maxChatMessages)
        {
            chatMessages.RemoveAt(0);
        }
        chatMessages.Add(m);
    }

    void Connect()
    {
        //Ændre Connect String når Spillet er opdateret
        PhotonNetwork.ConnectUsingSettings("3.0.0");
    }

    void OnGUI()
    {
        GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());

        if(PhotonNetwork.connected == false && connecting == false)
        {
            GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.BeginVertical();
            GUILayout.FlexibleSpace();

			GUILayout.BeginHorizontal();
			GUILayout.Label("Sorry Multiplayer is currenly being maintained and is therefore deactivated");
			GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
                GUILayout.Label("Username: ");
                PhotonNetwork.player.name = GUILayout.TextField(PhotonNetwork.player.name);
            GUILayout.EndHorizontal();
            
            if (GUILayout.Button("Singelplayer"))
            {
				//Application.LoadLevel ("Singelplayer");
                connecting = true;
                PhotonNetwork.offlineMode = true;
                OnJoinedLobby();
            }

            if(GUILayout.Button("Multiplayer"))
            {
				
				/*
				connecting = true;
                Connect();
                */

            }

			GUILayout.Label (" ");

			if (GUILayout.Button("Traning Grounds"))
			{
				Application.LoadLevel ("Traning Grounds");
				connecting = true;
				PhotonNetwork.offlineMode = true;
				OnJoinedLobby();
			}

			if (GUILayout.Button("Settings"))
			{
				gameOptions.SetActive (true);
			}

            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }
        if(PhotonNetwork.connected == true && connecting == false)
        {
            GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
            GUILayout.BeginVertical();
            GUILayout.FlexibleSpace();

            foreach(string msg in chatMessages)
            {
                GUILayout.Label(msg);
            }
            

            GUILayout.EndVertical();
            GUILayout.EndArea();
        }
    }

    void OnJoinedLobby()
    {
        Debug.Log("Joined Lobby, joining random room");
        if (offlineMode == true)
        {

            Debug.Log("offlineMode == true");
            PhotonNetwork.Instantiate("_ENEMY_AI", enemySpawnSpot.transform.position, enemySpawnSpot.transform.rotation, 0);
            //GameObject.Instantiate(enemyAI, enemySpawnSpot.transform.position, enemySpawnSpot.transform.rotation);
            //Object.Instantiate(enemyAI, transform.position, transform.rotation, null);

        }
        else
        {
            PhotonNetwork.JoinRandomRoom();
        }
        
    }

    void OnPhotonRandomJoinFailed()
    {
        Debug.Log("Join random room failed, creating new Room");
        PhotonNetwork.CreateRoom("Room " + Random.Range(1, 1000).ToString());
    }

        /*
        if (offlineMode == true)
        {
            Debug.Log("offlineMode == true");
            PhotonNetwork.Instantiate("_ENEMY_AI", enemySpawnSpot.transform.position, enemySpawnSpot.transform.rotation, 0);
            //GameObject.Instantiate(enemyAI, enemySpawnSpot.transform.position, enemySpawnSpot.transform.rotation);
            //Object.Instantiate(enemyAI, transform.position, transform.rotation, null);
        }
        */

    void OnJoinedRoom()
    {
        Debug.Log("Joined Room " + PhotonNetwork.room.name);
        AddChatMessage("Joined Room " + PhotonNetwork.room.name);

        connecting = false;
        Debug.Log("Connecting = false");

        if (PhotonNetwork.offlineMode == true)
        {
            Debug.Log("offlineMode == true");
            PhotonNetwork.Instantiate("_ENEMY_AI", enemySpawnSpot.transform.position, enemySpawnSpot.transform.rotation, 0);
            //GameObject.Instantiate(enemyAI, enemySpawnSpot.transform.position, enemySpawnSpot.transform.rotation);
            //Object.Instantiate(enemyAI, transform.position, transform.rotation, null);
        }

        SpawnMyPlayer();
    }

    void SpawnMyPlayer()
    {

        AddChatMessage(PhotonNetwork.player.name + " joined the game");

        if (spawnSpots == null)
        {
            Debug.LogError("No spawnspots!");
            return;
        }
        SpawnSpot mySpawnSpot = spawnSpots[Random.Range(0, spawnSpots.Length)];
		GameObject myPlayerGO = (GameObject)PhotonNetwork.Instantiate("_PLAYER_CONTROLLER", mySpawnSpot.transform.position, mySpawnSpot.transform.rotation, 0);
        
        standbyCamera.SetActive(false);

        myPlayerGO.GetComponent<AeroplaneController>().enabled = true;
        myPlayerGO.GetComponent<AeroplaneControlSurfaceAnimator>().enabled = true;
        myPlayerGO.GetComponent<AeroplaneUserControl2Axis>().enabled = true;
        myPlayerGO.GetComponent<AeroplaneAudio>().enabled = true;
        myPlayerGO.GetComponent<ObjectResetter>().enabled = true;
        myPlayerGO.GetComponent<Animator>().enabled = true;
        myPlayerGO.GetComponent<LandingGear>().enabled = true;
        myPlayerGO.GetComponentInChildren<Camera>().enabled = true;

		/*
		* myPlayerGO.transform.FindChild ("ExplosionSensor").gameObject.SetActive (true);
		* myPlayerGO.transform.FindChild ("ExplosionSensor (1)").gameObject.SetActive (true);
		* myPlayerGO.transform.FindChild ("ExplosionSensor (2)").gameObject.SetActive (true);
		* myPlayerGO.transform.FindChild ("ExplosionSensor (3)").gameObject.SetActive (true);
		* myPlayerGO.transform.FindChild ("ExplosionSensor (4)").gameObject.SetActive (true);
		* myPlayerGO.transform.FindChild ("ExplosionSensor (5)").gameObject.SetActive (true);
		* myPlayerGO.transform.FindChild ("ExplosionSensor (6)").gameObject.SetActive (true);
		* myPlayerGO.transform.FindChild ("ExplosionSensor (7)").gameObject.SetActive (true);
		* myPlayerGO.transform.FindChild ("ExplosionSensor (8)").gameObject.SetActive (true);
		* myPlayerGO.transform.FindChild ("ExplosionSensor (9)").gameObject.SetActive (true);
		* myPlayerGO.transform.FindChild ("ExplosionSensor (10)").gameObject.SetActive (true);
		* 
        * myPlayerGO.transform.FindChild("Main Camera").gameObject.SetActive(true);
        */
    }

    void Update()
    {
        if(respawnTimer > 0)
        {
            respawnTimer -= Time.deltaTime;
            if (respawnTimer <= 0)
            {
                SpawnMyPlayer();
            }
        }

    }
}
