﻿using UnityEngine;
using System.Collections;

public class Quit : MonoBehaviour {

	// Use this for initialization
	void OnGUI () {
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.BeginVertical();
			GUILayout.FlexibleSpace();

			GUILayout.BeginHorizontal();
			GUILayout.Label("Do you really want to quit?");
			GUILayout.EndHorizontal();

			if (GUILayout.Button("Yes!"))
			{
				Application.Quit ();
			}

			if(GUILayout.Button("No!"))
			{
				Application.CancelQuit ();
			}

			GUILayout.FlexibleSpace();
			GUILayout.EndVertical();
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			GUILayout.EndArea();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
