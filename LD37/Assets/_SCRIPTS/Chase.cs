﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;

public class Chase : MonoBehaviour {
    public GameObject _ScriptsGO;
    WaypointProgressTracker pTracker;

    void Start()
    {
        pTracker = transform.parent.parent.GetComponent<WaypointProgressTracker>();
        pTracker.circuit = GameObject.FindWithTag("AIWaypoints").GetComponent<WaypointCircuit>();
        _ScriptsGO = GameObject.FindWithTag("_ScriptsGO");

    }

    void OnTriggerEnter(Collider playerPlane)
    {
        Debug.Log("Enemy Triggered");

        if(playerPlane.tag == "PlayerCollider")
        {
            pTracker.circuit = GameObject.FindWithTag("ChaseCircuit").GetComponent<WaypointCircuit>();
            _ScriptsGO.GetComponent<NetworkManager>().AddChatMessage("Enemy target locked sight on you!");
        }
    }
}
