﻿using UnityEngine;
using System.Collections;

public class DestroySelf : MonoBehaviour {

    public float TimeUntilDestruction;

	// Use this for initialization
	void Start () {
        //Invoke starter "SelfDestruct" functionen, efter "TimeUntilDestruction"
        Invoke("SelfDestruct", TimeUntilDestruction);
	}
	
	void SelfDestruct () {
        PhotonNetwork.Destroy(gameObject);
	}
}
