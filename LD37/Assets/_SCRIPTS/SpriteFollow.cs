﻿using UnityEngine;
using System.Collections;

public class SpriteFollow : MonoBehaviour {

    //Objektet som skal følges
    public Transform Target;

    void LateUpdate()
    {
        //Target.position.x && Target.position.z gør at Spriten følger Target's position, men
        //transform.position.y gør at den altid har samme Y aksel så det ikke ser dumt ud på minimappet
        transform.position = new Vector3(Target.position.x, transform.position.y, Target.position.z);
    }
}
