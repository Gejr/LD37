﻿using UnityEngine;
using System.Collections;

public class NetworkCharacter : Photon.MonoBehaviour {

    Vector3 realPosition = Vector3.zero;
    Quaternion realRotation = Quaternion.identity;
    float lastUpdateTime;

    bool gotFirstUpdate = false;

    void Update()
    {
        if(photonView.isMine)
        {
            //Gør intet -- AeroplaneController bevæger os
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, realPosition, 0.1f);
            transform.rotation = Quaternion.Lerp(transform.rotation, realRotation, 0.1f);
        }
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            //Det er er VORES player. Vi sender vores faktiske position til netværket

            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        }
        else
        {
            //Det er en andes player. Vi modtager deres faktiske position (fra et par millisekunder siden)
            // og opdatere VORES version af den andens spiller.

            //Hvis der er lag så, spriger vi LERP over
            if (gotFirstUpdate == false)
            {
                transform.position = realPosition;
                transform.rotation = realRotation;
                gotFirstUpdate = true;
            }

            realPosition = (Vector3)stream.ReceiveNext();
            realRotation = (Quaternion)stream.ReceiveNext();
        }
    }
}
