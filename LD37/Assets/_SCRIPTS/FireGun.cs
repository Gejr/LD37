﻿using UnityEngine;
using System.Collections;

public class FireGun : MonoBehaviour {

    public GameObject bulletObject;
    public float fireRate;

    float nextFire = 0.0f;
    GameObject jetType;

    //Find jetType'en der bruger dette gevær
    void Start()
    {
        //Er det Player-type eller Enemy-type
        jetType = transform.parent.parent.gameObject;
    }

    //Tjekker om alle kravende er opfyldte, og hvis de er, så kan du skyde
    void FixedUpdate()
    {



        //Hvis geværet hører til Player, og der venstreklikkes på kan den skyde
        if (jetType.tag == "Player" && Input.GetMouseButton(0))
        {
            ConsiderFiring();
        }
        else
        {
            //Hvis geværet ikke hører til Player,så hører det til Enemy
            //Laver en Raycast for at se om den er tæt på Player
            if(StandardRaycast.GetTagFromRaycast(transform.position, transform.forward, 100000) == "EnemyAI")
            {
                ConsiderFiring();
            }
        }
    }

    void ConsiderFiring()
    {
        if(Time.time > nextFire)
        {
            //Laver et skyd og sender det frem ad
            GameObject newBullet = PhotonNetwork.Instantiate("Bullet", transform.position, transform.rotation, 0) as GameObject;
            newBullet.GetComponent<Rigidbody>().velocity = transform.forward * 500;

            //Resetter tilladelsen for at den kan skyde igen
            nextFire = Time.time + fireRate;
        }
    }
}
